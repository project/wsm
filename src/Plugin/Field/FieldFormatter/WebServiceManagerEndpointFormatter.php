<?php

namespace Drupal\wsm\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\wsm\Services\WebServiceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "wsm_endpoint",
 *   label = @Translation("WebService Enpoint"),
 *   description = @Translation("WebService Endpoint formatter with API call"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class WebServiceManagerEndpointFormatter extends EntityReferenceFormatterBase {

  /**
   * The WebService Manager service.
   *
   * @var \Drupal\wsm\Services\WebServiceManager
   */
  protected $manager;

  /**
   * Constructs a WebServiceManager Endpoint Formatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\wsm\Services\WebServiceManager $manager
   *   The WebService Manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    WebServiceManager $manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

     $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('wsm.webservice_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      // Call to the API.
      try {
        $api_res_array = $this->manager->endpointCall($entity);
        // Render content.
        $elements[$delta] = [
          '#theme' => 'endpoint_reference_formatter',
          '#fields' => $api_res_array,
        ];
      }
      catch (\Exception $exception) {
        $elements[$delta] = [
          '#theme' => 'endpoint_reference_formatter',
          '#fields' => [],
        ];
      }
    }

    return $elements;
  }

}
