<?php

namespace Drupal\wsm;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for params entity.
 */
interface ParamInterface extends ConfigEntityInterface {

  /**
   * Returns the param name.
   *
   * @return string
   *   The param name.
   */
  public function getName();

  /**
   * Sets the param name.
   *
   * @param string $name
   *   The param name.
   */
  public function setName($name);

  /**
   * Returns the param associated endpoint.
   *
   * @return string
   *   The endpoint machine name.
   */
  public function getEndpoint();

  /**
   * Sets the associated endpoint.
   *
   * @param string $endpoint
   *   The param name.
   */
  public function setEndpoint($endpoint);

  /**
   * Returns the param value.
   *
   * @return string
   *   The param value.
   */
  public function getvalue();

  /**
   * Sets the param name.
   *
   * @param string $name
   *   The param name.
   */
  public function setValue($value);

}
