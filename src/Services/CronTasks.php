<?php

namespace Drupal\wsm\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\wsm\Services\WebServiceManager;

/**
 * Class for web service retriever module cron task.
 */
class CronTasks {

  /**
   * Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

    /**
     * WebService Manager Service.
     *
     * @var \Drupal\wsm\Services\WebServiceManager
     */
    protected $manager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, WebServiceManager $manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->manager = $manager;
  }

  /**
   * Call all endpoint to import contents or save results in files.
   */
  public function endpointsCall() {
    // Prepare conditions for query.
    $query = $this->entityTypeManager->getStorage('endpoint')->getQuery();
    $conditions = $query->orConditionGroup()
      ->condition('saveEntity', TRUE)
      ->condition('saveFile', TRUE);

    $endpoints_ids = $query->condition($conditions)
      ->execute();
    if (count($endpoints_ids) > 0) {
      $endpoints = $this->entityTypeManager->getStorage('endpoint')->loadMultiple($endpoints_ids);
      foreach ($endpoints as $endpoint) {
          $this->manager->endpointCall($endpoint);
      }
    }
  }

}
