<?php

namespace Drupal\wsm\Services;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class for endpoint actions.
 */
class WebServiceManager {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * File System service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructor.
   */
  public function __construct(
      EntityTypeManagerInterface $entityTypeManager,
      EntityFieldManagerInterface $entityFieldManager,
      FileSystemInterface $fileSystem,
      FileRepositoryInterface $fileRepository,
      RouteMatchInterface $routeMatch,
      ClientInterface $client)
  {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->fileSystem = $fileSystem;
    $this->fileRepository = $fileRepository;
    $this->routeMatch = $routeMatch;
    $this->httpClient = $client;
  }

  /**
   * Function to transform utf-16 string to utf-8 one.
   *
   * @param string $utf16_string
   *   The utf-16 string to transform.
   *
   * @return string
   *   The utf-8 value of the string..
   */
  public function utf162Utf8($utf16_string) {
    // [U+D800 - U+DBFF][U+DC00 - U+DFFF]|[U+0000 - U+FFFF]
    $regex = '/\\\u([dD][89abAB][\da-fA-F]{2})\\\u([dD][c-fC-F][\da-fA-F]{2})|\\\u([\da-fA-F]{4})/sx';
    $encodedValue = preg_replace_callback($regex, function ($matches) {
      if (isset($matches[3])) {
        $cp = hexdec($matches[3]);
      }
      else {
        $lead = hexdec($matches[1]);
        $trail = hexdec($matches[2]);

        // http://unicode.org/faq/utf_bom.html#utf16-4.
        $cp = ($lead << 10) + $trail + 0x10000 - (0xD800 << 10) - 0xDC00;
      }

      // https://tools.ietf.org/html/rfc3629#section-3.
      // Characters between U+D800 and U+DFFF are not allowed in UTF-8.
      if ($cp > 0xD7FF && 0xE000 > $cp) {
        return '';
      }

      // https://github.com/php/php-src/blob/php-5.6.4/ext/standard/html.c#L471.
      // php_utf32_utf8(unsigned char *buf, unsigned k).
      if ($cp < 0x80) {
        return chr($cp);
      }
      elseif ($cp < 0xA0) {
        return chr(0xC0 | $cp >> 6) . chr(0x80 | $cp & 0x3F);
      }

      return html_entity_decode('&#' . $cp . ';');
    }, $utf16_string);

    return $encodedValue;
  }

  /**
   * Function for source field path value get.
   *
   * @param string $path
   *   The path to get the value in.
   * @param array $api_res_array
   *   The array of api call results.
   *
   * @return string|array
   *   The value or an array of values.
   */
  protected function getSourceFieldValue($path, array $api_res_array) {
    $source_path_arr = explode("/", $path);
    $api_res_value = $api_res_array;
    foreach ($source_path_arr as $source_path) {
      if ($source_path == '*') {
        // Recursive call on levels down to the wildcard.
        $wildcard_res_arr = [];
        $sub_path_arr = explode('*', $path, 2);
        if (!empty($api_res_value)) {
          foreach ($api_res_value as $cur_value) {
            if (!empty($sub_path_arr[1])) {
              $wildcard_res_arr[] = $this->getSourceFieldValue(substr($sub_path_arr[1], 1), $cur_value);
            }
            else {
              $wildcard_res_arr[] = $cur_value;
            }
          }
        }
      }
      else {
        $api_res_value = $api_res_value[$source_path] ?? NULL;
      }
    }
    return $api_res_value;
  }

  /**
   * Function for image management.
   *
   * @param string $url
   *   The url of the file to retrieve.
   *
   * @return \Drupal\file\FileInterface
   *   The drupal file entity.
   */
  protected function createImage($url) {
    // @todo Path as endpont parameter ?
    $path = parse_url($url, PHP_URL_PATH);
    $extension = pathinfo($path, PATHINFO_EXTENSION);
    $filename = pathinfo($path, PATHINFO_FILENAME);
    $file_path = 'public://wsm/images/';

    if (strpos($filename, '?') === 0 || empty($filename)) {
      $url_array = explode("/", $url);
      $filename = ($url_array[4] ?? bin2hex(random_bytes(5))) . "_" . $filename;
    }
    $filename = str_replace(['?', '=', '&'], "_", $filename);
    if (strlen($filename) > 220) {
      $filename = substr($filename, 0, 220);
    }
    $filename .= (!empty($extension) ? "." . $extension : "");

    $existing_files = $this->entityTypeManager->getStorage('file')->loadByProperties(['uri' => $file_path . $filename]);
    if (count($existing_files)) {
      $existing = reset($existing_files);
      return $existing;
    }

    try {
        $response = $this->httpClient->get($url);
        $image = $response->getBody()->getContents();
    } catch (\Exception $exp) {
        $image = FALSE;
    }
    if ($image !== FALSE) {
      $this->fileSystem->prepareDirectory($file_path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $file = $this->fileRepository->writeData($image, $file_path . $filename);
      return $file;
    }

    return NULL;
  }

  /**
   * Function to save all objects in results to a Drupal entity.
   *
   * @param \Drupal\wsm\Entity\Endpoint $entity
   *   The endpoint entity called.
   * @param array $api_res_array
   *   The results of the API call.
   *
   * @return array
   *   The array of internal ids of created/updated entities.
   */
  protected function saveEntity($entity, $api_res_array) {
    $entity_type = $entity->getSaveInEntityType();
    $entity_bundle = $entity->getSaveInEntityBundle();
    $parent_element = $entity->getEntityParentElement();
    $update_existing = $entity->getEntityUpdate();
    $field_mappings = $entity->getFieldMappings();
    $fields = [];
    $importedEntities = [];

    if (!empty($parent_element) && !empty($api_res_array[$parent_element])) {
      $api_res_array = $api_res_array[$parent_element];
    }

    // We can receive only one entity or a list of entity.
    // Fro GraphQL, entity type is included in response.
    $api_res_keys = array_keys($api_res_array);
    if (!(count($api_res_keys) == 1 || is_int($api_res_keys[0]))) {
      $api_res_array = [$api_res_array];
    }

    $unique_id = NULL;
    $unique_field = NULL;
    $entity_fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $entity_bundle ?? $entity_type);
    foreach ($api_res_array as $api_res) {
      foreach ($field_mappings as $field_mapping) {
        $field_mapping_entity = $this->entityTypeManager->getStorage('field_mapping')->load($field_mapping);
        // Manage value getter if source field is a path.
        $value = $field_mapping_entity->getSourceField();
        if (!$field_mapping_entity->getIsValueField()) {
          $value = $this->getSourceFieldValue($value, $api_res);
        }
        // Check necessity to transform to lower.
        if ($field_mapping_entity->getIsToLowerString()) {
          if (is_array($value)) {
            for ($i = 0; $i < count($value); $i++) {
              $value[$i] = strtolower($value[$i]);
            }
          }
          else {
            $value = strtolower($value);
          }
        }

        /* Specific actions regarding field type */
        // @todo Manage more destination field type to alter value.
        if (!empty($entity_fields[$field_mapping_entity->getDestinationField()]) && !empty($value)) {
          $field_type = $entity_fields[$field_mapping_entity->getDestinationField()]->getType();
          switch ($field_type) {
            case 'datetime':
              if (is_array($value)) {
                for ($i = 0; $i < count($value); $i++) {
                  $date = new DrupalDateTime($value[$i]);
                  $value[$i] = $date->format("Y-m-d\TH:i:s");
                }
              }
              else {
                $date = new DrupalDateTime($value);
                $value = $date->format("Y-m-d\TH:i:s");
              }
              break;

            case 'image':
              if (is_array($value)) {
                for ($i = 0; $i < count($value); $i++) {
                  $value[$i] = $this->createImage($value[$i]);
                }
              }
              else {
                $value = $this->createImage($value);
              }
              break;

            case 'entity_reference':
              $is_refecencebydistantid = $field_mapping_entity->getIsReferenceByDistantId();
              $field_refecenceoriginalid_name = $field_mapping_entity->getReferenceOriginalIdField();
              /* @var \Drupal\field\Entity\FieldConfig $field */
              if ($is_refecencebydistantid) {
                $field = $entity_fields[$field_mapping_entity->getDestinationField()];

                if ($field->getSetting("handler") === 'default:taxonomy_term') {
                  $vocabulary = array_shift($field->getSetting("handler_settings")['target_bundles']);
                  /* @var \Drupal\taxonomy\Entity\Term $term */
                  $term = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
                    'vid' => $vocabulary,
                    ($field_refecenceoriginalid_name ?? 'field_origina_id') => $value
                  ]);
                  if (!empty($term)) {
                    $value = $term;
                  } else {
                    $value = NULL;
                  }
                }
              }
              break;
          }
        }

        /* Specific actions regarding destination field name */
        switch ($field_mapping_entity->getDestinationField()) {
          case 'created':
            $value = str_replace('/', '-', $value);
            $date = new DrupalDateTime($value);
            $value = $date->format("U");
            break;
        }

        $fields[$field_mapping_entity->getDestinationField()] = $value;
        if ($field_mapping_entity->getIsIdField()) {
          $unique_id = $api_res[$field_mapping_entity->getSourceField()];
          $unique_field = $field_mapping_entity->getDestinationField();
        }
      }

      $entity = NULL;
      // Load eventual existing entity.
      if (!empty($unique_id)) {
        try {
          $entity = $this->entityTypeManager->getStorage($entity_bundle)->loadByProperties([$unique_field => $unique_id]);
        }
        catch (\Exception $e) {
          $entity = $this->entityTypeManager->getStorage($entity_type)->loadByProperties([$unique_field => $unique_id]);
        }
        // Assign fields to the entity.
        if (!empty($entity) && $update_existing) {
          $entity = reset($entity);
          foreach ($fields as $field_name => $value) {
            $entity->set($field_name, $value);
          }
          try {
            $entity->save();
            $importedEntities[] = $entity->id();
          }
          catch (\Exception $e) {
            break;
          }
        }
      }

      // If no entity exists, create new one.
      if (empty($entity)) {
        try {
          $entity = $this->entityTypeManager->getStorage($entity_bundle)->create($fields);
          $entity->save();
          $importedEntities[] = $entity->id();
        }
        catch (\Exception $e) {
          if ($entity_type === 'taxonomy_term') {
            $fields['vid'] = $entity_bundle;
            if (!empty($fields['name'])) {
              $entity = $this->entityTypeManager->getStorage($entity_type)->create($fields);
              $entity->save();
              $importedEntities[] = $entity->id();
            }
          } elseif($entity_type === 'node') {
            if (!empty($entity_bundle)) {
              $fields['type'] = $entity_bundle;
            }
            if (!empty($fields['title'])) {
              $entity = $this->entityTypeManager->getStorage($entity_type)->create($fields);
              $entity->save();
              $importedEntities[] = $entity->id();
            }
          } else {
            if (!empty($entity_bundle)) {
              $fields['type'] = $entity_bundle;
            }

            $entity = $this->entityTypeManager->getStorage($entity_type)->create($fields);
            $entity->save();
            $importedEntities[] = $entity->id();
          }
        }
      }
    }

    return $importedEntities;
  }

  /**
   * Function to save a file from API url to a Drupal file entity.
   *
   * @param \Drupal\wsm\Entity\Endpoint $entity
   *   The endpoint entity called.
   * @param array $api_res_array
   *   The results of the API call.
   */
  protected function saveFile($entity, $api_res_array) {
    $file_path = "public://wsm/endpoint_files/";
    $file_name = $entity->id();
    $this->fileSystem->prepareDirectory($file_path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->fileRepository->writeData(json_encode($api_res_array), $file_path . $file_name);
  }

  /**
   * Function for API call and actions regarding enpoint settings.
   */
  public function endpointCall($entity) {
    $method = $entity->getMethod();
    $response_type = $entity->getResponseType();
    $url = $entity->getEndpointBaseUrl();
    $last_path_entity_id = $entity->getLastPathEntityId();
    $endpoint_headers = $entity->getHeaders();
    $endpoint_params = $entity->getParams();
    $ignore_empty = $entity->getIgnoreEmpty();
    $save_into_entity = $entity->getSaveEntity();
    $save_into_file = $entity->getSaveFile();
    $delete_existing = $entity->getEntityDelete();
    $field_mappings = $entity->getFieldMappings();

    // Call to the API.
    try {
      $client = \Drupal::httpClient();

      if ($last_path_entity_id) {
        foreach ($field_mappings as $field_mapping) {
          $field_mapping_entity = $this->entityTypeManager->getStorage('field_mapping')->load($field_mapping);
          if ($field_mapping_entity->getIsIdField()) {
            $destimationField = $field_mapping_entity->getDestinationField();
            $node = $this->routeMatch->getParameter('node');
            if($node) {
              $destimationId = $node->get($destimationField)->first()->getValue()['value'];
              $url .= '/' . $destimationId;
            }
          }
        }
      }

      $headers = [];
      foreach ($endpoint_headers as $endpoint_header) {
        $header_entity = $this->entityTypeManager->getStorage('header')->load($endpoint_header);
        $headers[$header_entity->getName()] = $header_entity->getValue();
      }

      $params = [];
      foreach ($endpoint_params as $endpoint_param) {
        $param_entity = $this->entityTypeManager->getStorage('param')->load($endpoint_param);
        $params[$param_entity->getName()] = $param_entity->getValue();
      }

      $options = $params + [
        'headers' => $headers,
      ];

      if ($method == 'POST') {
        $request = $client->post($url, $options);
      }
      else {
        $request = $client->get($url, $options);
      }

      $res_txt = $request->getBody()->getContents();
      switch ($response_type) {
        case "json":
          $api_res_array = json_decode($this->utf162Utf8($res_txt), TRUE);
          break;

        case 'xml':
          $api_res_obj = simplexml_load_string($res_txt);
          $api_res_json = json_encode($api_res_obj);
          $api_res_array = json_decode($api_res_json, TRUE);
          break;

        default:
          $api_res_array = [];
      }
    }
    catch (\Exception $exp) {
      $api_res_array = [];
    }

    // Saving into entity.
    if ($save_into_entity && !empty($api_res_array)) {
      $savedIds = $this->saveEntity($entity, $api_res_array);
      if ($delete_existing && (!empty($savedIds) || !$ignore_empty)) {
        try {
          $entity_type = $entity->getSaveInEntityType();
          $entity_bundle = $entity->getSaveInEntityBundle();
          $storageHandler = $this->entityTypeManager->getStorage($entity_type);
          $entityKeys = $this->entityTypeManager->getDefinition($entity_type)->getKeys();

          $query = $storageHandler->getQuery()
            ->condition($entityKeys['id'], $savedIds, 'NOT IN');
          if ($entity_type != $entity_bundle && !empty($entityKeys['bundle'])) {
            $query->condition($entityKeys['bundle'], $entity_bundle);
          }

          foreach ($field_mappings as $field_mapping) {
            $field_mapping_entity = $this->entityTypeManager->getStorage('field_mapping')->load($field_mapping);
            if ($field_mapping_entity->getUseOnDelete()) {
              $destimationField = $field_mapping_entity->getDestinationField();
              $value = $field_mapping_entity->getSourceField();
              $query->condition($destimationField, $value);
            }
          }

          $toDeleteIds = $query->accessCheck(FALSE)->execute();

          $toDeleteEntities = $storageHandler->loadMultiple($toDeleteIds);
          $storageHandler->delete($toDeleteEntities);
        }
        catch (\Exception $exp) {}
      }
    }

    // Saving into file.
    if ($save_into_file && (!empty($api_res_array) || !$ignore_empty)) {
      $this->saveFile($entity, $api_res_array);
    }

    return $api_res_array;
  }

}
