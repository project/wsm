<?php

namespace Drupal\wsm;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for headers entity.
 */
interface HeaderInterface extends ConfigEntityInterface {

  /**
   * Returns the header name.
   *
   * @return string
   *   The header name.
   */
  public function getName();

  /**
   * Sets the header name.
   *
   * @param string $name
   *   The header name.
   */
  public function setName($name);

  /**
   * Returns the header associated endpoint.
   *
   * @return string
   *   The endpoint machine name.
   */
  public function getEndpoint();

  /**
   * Sets the associated endpoint.
   *
   * @param string $endpoint
   *   The header name.
   */
  public function setEndpoint($endpoint);

  /**
   * Returns the header value.
   *
   * @return string
   *   The header value.
   */
  public function getvalue();

  /**
   * Sets the header name.
   *
   * @param string $name
   *   The header name.
   */
  public function setValue($value);

}
