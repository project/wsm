<?php

namespace Drupal\wsm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\wsm\EndpointInterface;

/**
 * Provides route responses for wsm.module.
 */
class WebServiceManagerController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\wsm\EndpointInterface $endpoint
   *   The endpoint.
   *
   * @return string
   *   The endpoint label as a render array.
   */
  public function endpointTitle(EndpointInterface $endpoint) {
    return $this->t('Edit %label endpoint', array('%label' => $endpoint->label()));
  }

}
