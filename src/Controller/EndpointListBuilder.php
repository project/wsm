<?php

namespace Drupal\wsm\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of endpoint entities.
 *
 * @see \Drupal\wsm\Entity\Endpoint
 */
class EndpointListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Endpoint');
    $header['description'] = [
      'data' => $this->t('Description'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\wsm\EndpointInterface $endpoint */
    $endpoint = $entity;
    $row['label'] = $endpoint->label();
    $row['description']['data'] = ['#markup' => $endpoint->getDescription()];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = $this->t('Edit endpoint');
    }

    $operations['headers'] = [
      'title' => $this->t('Manage headers'),
      'weight' => 10,
      'url' => Url::fromRoute('entity.headers', [
        'endpoint' => $entity->id(),
      ]),
    ];

    $operations['params'] = [
      'title' => $this->t('Manage params'),
      'weight' => 10,
      'url' => Url::fromRoute('entity.params', [
        'endpoint' => $entity->id(),
      ]),
    ];

    $operations['field_mappings'] = [
      'title' => $this->t('Manage field mappings'),
      'weight' => 10,
      'url' => Url::fromRoute('entity.field_mappings', [
        'endpoint' => $entity->id(),
      ]),
    ];

    return $operations;
  }

}
