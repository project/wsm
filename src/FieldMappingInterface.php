<?php

namespace Drupal\wsm;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for field mapping entity.
 */
interface FieldMappingInterface extends ConfigEntityInterface {

  /**
   * Returns the field mapping associated endpoint.
   *
   * @return string
   *   The endpoint machine name.
   */
  public function getEndpoint();

  /**
   * Sets the associated endpoint.
   *
   * @param string $endpoint
   *   The param name.
   */
  public function setEndpoint($endpoint);

  /**
   * Returns the field mapping source field.
   *
   * @return string
   *   The field mapping source field.
   */
  public function getSourceField();

  /**
   * Sets the field mapping source field.
   *
   * @param string $src_field
   *   The field mapping source field.
   */
  public function setSourceField($src_field);

  /**
   * Returns the field mapping destination field.
   *
   * @return string
   *   The field mapping destination field.
   */
  public function getDestinationField();

  /**
   * Sets the field mapping destination field.
   *
   * @param string $dest_field
   *   The field mapping destination field.
   */
  public function setDestinationField($dest_field);

  /**
   * Returns the is id field flag.
   *
   * @return bool
   *   The  is id field flag.
   */
  public function getIsIdField();

  /**
   * Sets the is id field flag.
   *
   * @param bool $is_id
   *   The is id flag.
   */
  public function setIsIdField($is_id);

  /**
   * Returns the is value field flag.
   *
   * @return bool
   *   The  is value field flag.
   */
  public function getIsValueField();

  /**
   * Sets the is value field flag.
   *
   * @param bool $is_value
   *   The is value flag.
   */
  public function setIsValueField($is_value);

  /**
   * Returns the is to lower string flag.
   *
   * @return bool
   *   The  is value field flag.
   */
  public function getIsToLowerString();

  /**
   * Sets the is to lower string flag.
   *
   * @param bool $is_tolowerstring
   *   The is to lower string flag.
   */
  public function setIsToLowerString($is_tolowerstring);

  /**
   * Returns the use on delete flag.
   *
   * @return bool
   *   The use on delete field flag.
   */
  public function getUseOnDelete();

  /**
   * Sets the use on delete flag.
   *
   * @param bool $use_on_delete
   *   The use on delete flag.
   */
  public function setUseOnDelete($use_on_delete);

  /**
   * Returns the is reference by distant id flag.
   *
   * @return bool
   *   The is reference by distant id flag.
   */
  public function getIsReferenceByDistantId();

  /**
   * Sets the is reference by distant id flag.
   *
   * @param bool $is_referencebydistantid
   *   The is reference by distant id flag.
   */
  public function setIsReferenceByDistantId($is_referencebydistantid);

  /**
   * Returns the reference original id field name.
   *
   * @return string
   *   The reference original id field name.
   */
  public function getReferenceOriginalIdField();

  /**
   * Sets the reference origianl id field name.
   *
   * @param string $reference_origial_id_field
   *   The reference original id field name.
   */
  public function setReferenceOriginalIdField($reference_origial_id_field);
}
