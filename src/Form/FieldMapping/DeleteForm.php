<?php

namespace Drupal\wsm\Form\FieldMapping;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove a field mapping from a endpoint.
 */
class DeleteForm extends EntityConfirmFormBase {

  /**
   * The endpoints that the field mapping is applied to.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  protected $endpoint;

  /**
   * The field mapping to be removed from the endpoint.
   *
   * @var \Drupal\wsm\FieldMappingInterface
   */
  protected $field_mapping;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());

    return $this->t('Are you sure you want to delete the @param field mapping from the %endpoint endpoint?', ['%endpoint' => $endpoint->label(), '@param' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.field_mappings', [
      'endpoint' => $this->entity->getEndpoint(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());
    $endpoint->removeFieldMapping($this->entity->id());
    $this->entity->delete();

    $this->messenger()->addMessage($this->t('Entity %id has been deleted.', array('%id' => $this->entity->id())));

    $this->logger('endpoint')->notice('The field mapping %id has been deleted in the @endpoint endpoint.', [
      '%id' => $this->entity->id(),
      '@endpoint' => $endpoint->label(),
    ]);

    $form_state->setRedirect('entity.field_mappings', [
      'endpoint' => $endpoint->id(),
    ]);

  }

}
