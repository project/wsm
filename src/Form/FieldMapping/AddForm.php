<?php

namespace Drupal\wsm\Form\FieldMapping;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to apply field mapping to an endpoint.
 */
class AddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    return $actions;
  }

}
