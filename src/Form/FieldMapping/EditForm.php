<?php

namespace Drupal\wsm\Form\FieldMapping;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to edit field mapping.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save field mapping');

    $actions['delete']['#url']->setRouteParameter('endpoint', \Drupal::routeMatch()->getParameter('endpoint'));
    $actions['delete']['#value'] = $this->t('Delete field mapping');

    return $actions;
  }

}
