<?php

namespace Drupal\wsm\Form\FieldMapping;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wsm\EndpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an overview form for field mappings on an endpoint.
 */
class OverviewForm extends FormBase {

  /**
   * The endpoint to which the field mappings are applied to.
   *
   * @var \Drupal\wsm\FieldMappingInterface
   */
  private $endpoint;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "wsm_field_mapping_overview_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EndpointInterface $endpoint = NULL) {
    $this->endpoint = $endpoint;
    $endpoint_id = $this->endpoint->id();

    $form['params'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Source Field'),
          'colspan' => 2
        ],
        $this->t('Destination Field'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No field mappings added.'),
    ];
    $field_mappings = \Drupal::entityTypeManager()->getStorage('field_mapping')->loadMultiple($this->endpoint->getFieldMappings());

    foreach ($field_mappings as $field_mapping) {
      $key = $field_mapping->id();

      $form['params'][$key]['id'] = [
        '#plain_text' => (string) str_replace($endpoint_id . '__param_', '', $field_mapping->id()),
      ];

      $form['params'][$key]['src_field'] = [
        '#plain_text' => (string) $field_mapping->getSourceField(),
      ];

      $form['params'][$key]['dest_field'] = [
        '#plain_text' => (string) $field_mapping->getDestinationField(),
      ];

      $form['params'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];

      $form['params'][$key]['operations']['#links']['edit'] = [
        'title' => t('Edit'),
        'url' => Url::fromRoute('entity.field_mapping.edit_form', [
          'endpoint' =>  $this->endpoint->id(),
          'field_mapping' => $key,
        ]),
      ];

      $form['params'][$key]['operations']['#links']['delete'] = [
        'title' => t('Delete'),
        'url' => Url::fromRoute('entity.field_mapping.delete_form', [
          'endpoint' =>  $this->endpoint->id(),
          'field_mapping' => $key,
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->endpoint->save();
  }

}
