<?php

namespace Drupal\wsm\Form\FieldMapping;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base form for field mapping add and edit forms.
 */
class FormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\wsm\FieldMappingInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $endpoint_id = $this->getRouteMatch()->getParameter('endpoint');

    $form['src_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source field'),
      '#default_value' => $this->entity->getSourceField(),
      '#description' => $this->t("You can use path into response as source field. '*' wildcard is allowed to loop. Ex media/images/*/url."),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#field_prefix' => ($this->entity->isNew()) ? $endpoint_id . '__field_mapping_' : '',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\wsm\Entity\FieldMapping', 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['dest_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination field'),
      '#default_value' => $this->entity->getDestinationField(),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['is_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is unique id'),
      '#default_value' => $this->entity->getIsIdField(),
    ];

    $form['is_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is field value'),
      '#default_value' => $this->entity->getIsValueField(),
    ];

    $form['is_tolowerstring'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save as lower case string'),
      '#default_value' => $this->entity->getIsToLowerString(),
    ];

    $form['use_on_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use as condition en delete process'),
      '#default_value' => $this->entity->getUseOnDelete(),
    ];

    $form['is_referencebydistantid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is a refecence field by distant object id'),
      '#default_value' => $this->entity->getIsReferenceByDistantId(),
    ];

    $form['reference_origial_id_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The entity distant object id field name'),
      '#default_value' => $this->entity->getReferenceOriginalIdField(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $endpoint_id = $this->getRouteMatch()->getParameter('endpoint');
    $field_mapping = $this->entity;

    if ($field_mapping->isNew()) {
      $field_mapping->id = $endpoint_id . '__field_mapping_' . $field_mapping->id();
      $field_mapping->setEndpoint($endpoint_id);

      $endpoint = $this->entityTypeManager->getStorage('endpoint')->load($endpoint_id);
      $endpoint->addFieldMapping($field_mapping->id());
      $endpoint->save();
    }

    $status = $field_mapping->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created new field mapping.'));
        $this->logger('wsm')->notice('Created new field mapping.');
        break;

      case SAVED_UPDATED:
        $this->messenger()->addMessage($this->t('Updated field mapping-'));
        $this->logger('wsm')->notice('Updated field mapping.');
        break;
    }

    $form_state->setRedirect('entity.field_mappings', ['endpoint' => $field_mapping->getEndpoint()]);
  }

  /**
   * Helper function to check whether an Endpoint configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('field_mapping')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
