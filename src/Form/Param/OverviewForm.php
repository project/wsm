<?php

namespace Drupal\wsm\Form\Param;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wsm\EndpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an overview form for params on an endpoint.
 */
class OverviewForm extends FormBase {

  /**
   * The endpoint to which the params are applied to.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  private $endpoint;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "wsm_param_overview_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EndpointInterface $endpoint = NULL) {
    $this->endpoint = $endpoint;
    $endpoint_id = $this->endpoint->id();

    $form['params'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Param'),
          'colspan' => 2
        ],
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No params added.'),
    ];
    $params = \Drupal::entityTypeManager()->getStorage('param')->loadMultiple($this->endpoint->getParams());

    foreach ($params as $param) {
      $key = $param->id();

      $form['params'][$key]['id'] = [
        '#plain_text' => (string) str_replace($endpoint_id . '__param_', '', $param->id()),
      ];

      $form['params'][$key]['label'] = [
        '#plain_text' => (string) $param->getName(),
      ];

      $form['params'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];

      $form['params'][$key]['operations']['#links']['edit'] = [
        'title' => t('Edit'),
        'url' => Url::fromRoute('entity.param.edit_form', [
          'endpoint' =>  $this->endpoint->id(),
          'param' => $key,
        ]),
      ];

      $form['params'][$key]['operations']['#links']['delete'] = [
        'title' => t('Delete'),
        'url' => Url::fromRoute('entity.param.delete_form', [
          'endpoint' =>  $this->endpoint->id(),
          'param' => $key,
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->endpoint->save();
  }

}
