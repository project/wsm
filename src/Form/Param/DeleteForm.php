<?php

namespace Drupal\wsm\Form\Param;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove a param from a endpoint.
 */
class DeleteForm extends EntityConfirmFormBase {

  /**
   * The endpoints that the param is applied to.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  protected $endpoint;

  /**
   * The param to be removed from the endpoint.
   *
   * @var \Drupal\wsm\ParamInterface
   */
  protected $param;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());

    return $this->t('Are you sure you want to delete the @param param from the %endpoint endpoint?', ['%endpoint' => $endpoint->label(), '@param' => $this->entity->getName()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.params', [
      'endpoint' => $this->entity->getEndpoint(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());
    $endpoint->removeParam($this->entity->id());
    $this->entity->delete();

    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', array('%label' => $this->entity->label())));

    $this->logger('endpoint')->notice('The param %label has been deleted in the @endpoint endpoint.', [
      '%label' => $this->entity->label(),
      '@endpoint' => $endpoint->label(),
    ]);

    $form_state->setRedirect('entity.params', [
      'endpoint' => $endpoint->id(),
    ]);

  }

}
