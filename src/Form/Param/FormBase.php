<?php

namespace Drupal\wsm\Form\Param;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wsm\EndpointInterface;

/**
 * Base form for param add and edit forms.
 */
class FormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\wsm\Paramnterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $endpoint_id = \Drupal::routeMatch()->getParameter('endpoint');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Param name'),
      '#default_value' => $this->entity->getName(),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#field_prefix' => ($this->entity->isNew()) ? $endpoint_id . '__param_' : '',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\wsm\Entity\Param', 'load']
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Param value'),
      '#default_value' => $this->entity->getValue(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $endpoint_id = \Drupal::routeMatch()->getParameter('endpoint');
    $param = $this->entity;

    if ($param->isNew()) {
      $param->id = $endpoint_id . '__param_' . $param->id();
      $param->setEndpoint($endpoint_id);

      $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($endpoint_id);
      $endpoint->addParam($param->id());
      $endpoint->save();
    }

    $status = $param->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created new param %name.', ['%name' => $param->getName()]));
        $this->logger('wsm')->notice('Created new param %name.', ['%name' => $param->getName()]);
        break;

      case SAVED_UPDATED:
        \Drupal::messenger()->addMessage($this->t('Updated param %name.', ['%name' => $param->getName()]));
        $this->logger('wsm')->notice('Updated endpoint %name.', ['%name' => $param->getName()]);
        break;
    }

    $form_state->setRedirect('entity.params', ['endpoint' => $param->getEndpoint()]);
  }

  /**
   * Helper function to check whether an Endpoint configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('param')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
