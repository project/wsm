<?php

namespace Drupal\wsm\Form\Param;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to edit param.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save param');

    $actions['delete']['#url']->setRouteParameter('endpoint', \Drupal::routeMatch()->getParameter('endpoint'));
    $actions['delete']['#value'] = $this->t('Delete param');

    return $actions;
  }

}
