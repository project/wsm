<?php

namespace Drupal\wsm\Form\Endpoint;

use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to add an Endpoint.
 */
class AddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    return $actions;
  }

}
