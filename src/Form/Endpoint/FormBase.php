<?php

namespace Drupal\wsm\Form\Endpoint;

use Drupal\wsm\Entity\Endpoint;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Config\Entity\ConfigEntityType;

/**
 * Base form for endpoint add and edit forms.
 */
class FormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['general_informations'] = [
      '#type' => 'details',
      '#title' => $this->t('General informations'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    $form['general_informations']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enpoint name'),
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Usually website name + service (e.g. Huffington Post - articles). This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['general_informations']['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\wsm\Entity\Endpoint', 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['general_informations']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->getDescription(),
      '#description' => $this->t('The text will be displayed on the <em>endpoint collection</em> page.'),
    ];

    $form['general_informations']['endpointBaseUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enpoint base URL'),
      '#default_value' => $this->entity->getEndpointBaseUrl(),
      '#description' => $this->t('Endpoint base URL, without any specific parameters (e.g. https://www.example.com/api/articles).'),
      '#required' => TRUE,
    ];

    $form['general_informations']['lastPathEntityId'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add distant entity id as last path of the endpoint.'),
      '#default_value' => $this->entity->getLastPathEntityId(),
    ];

    $form['general_informations']['responseType'] = [
      '#type' => 'select',
      '#title' => $this->t('Response type'),
      '#options' => [
        'json' => 'JSON',
        'xml' => 'XML',
      ],
      '#default_value' => $this->entity->getResponseType(),
      '#description' => $this->t('Enpoint response type (json, xml).'),
      '#required' => TRUE,
    ];

    $form['general_informations']['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method'),
      '#options' => [
        'GET' => 'GET',
        'POST' => 'POST',
      ],
      '#default_value' => $this->entity->getMethod(),
      '#description' => $this->t('Enpoint method (GET, POST, ...).'),
      '#required' => TRUE,
    ];

    $form['behaviour'] = [
      '#type' => 'details',
      '#title' => $this->t('Behaviour on response'),
      '#tree' => TRUE,
      '#open' => FALSE,
    ];

    $form['behaviour']['saveEntity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save entity on response in content type.'),
      '#default_value' => $this->entity->getSaveEntity(),
    ];

    $form['behaviour']['entities'] = [
      '#type' => 'details',
      '#title' => $this->t('Select types to save in.'),
      '#tree' => TRUE,
      '#open' => TRUE,
      '#attributes' => [
        'id' => $this->getFormId() . '_entity_group',
      ],
    ];

    $form['behaviour']['entities']['saveInEntityType'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type.'),
      '#options' => $this->getAvailableEntityTypes(),
      '#empty_option' => ' ',
      '#default_value' => $this->entity->getSaveInEntityType(),
      '#ajax' => [
        'callback' => [$this, 'updateEntityBundleField'],
        'event' => 'change',
        'wrapper' => $this->getFormId() . '_entity_group',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading...'),
        ],
      ],
    ];

    $form['behaviour']['entities']['saveInEntityBundle'] = [
      '#type' => 'select',
      '#options' => $this->getAvailableEntityBundle($form_state, $this->entity),
      '#empty_option' => ' ',
      '#title' => $this->t('Entity bundle.'),
      '#default_value' => $this->entity->getSaveInEntityBundle(),
    ];

    $form['behaviour']['entities']['entityParentElement'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parent Element for entity(ies'),
      '#default_value' => $this->entity->getEntityParentElement(),
      '#description' => $this->t('Key for parent element containing entities. Leave empty if no parent.'),
      '#required' => FALSE,
    ];

    $form['behaviour']['entities']['entityUpdate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update existing entities'),
      '#default_value' => $this->entity->getEntityUpdate(),
      '#required' => FALSE,
    ];

    $form['behaviour']['entities']['entityDelete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete existing entities if not in APi results'),
      '#default_value' => $this->entity->getEntityDelete(),
      '#required' => FALSE,
    ];

    $form['behaviour']['saveFile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save response in a file.'),
      '#default_value' => $this->entity->getSaveFile(),
    ];

    $form['behaviour']['ignoreEmpty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore actions if return is empty.'),
      '#default_value' => $this->entity->getIgnoreEmpty(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $endpoint = $this->entity;

    $endpoint->set('label',
      trim($form_state->getValue(['general_informations', 'label']))
    );
    $endpoint->set('id',
      $form_state->getValue(['general_informations', 'id'])
    );
    $endpoint->setDescription(
      $form_state->getValue(['general_informations', 'description'])
    );
    $endpoint->setOriginalId(
      $form_state->getValue(['general_informations', 'id'])
    );
    $endpoint->setEndpointBaseUrl(
      $form_state->getValue(['general_informations', 'endpointBaseUrl'])
    );
    $endpoint->setLastPathEntityId(
      $form_state->getValue(['general_informations', 'lastPathEntityId'])
    );
    $endpoint->setResponseType(
      $form_state->getValue(['general_informations', 'responseType'])
    );
    $endpoint->setMethod(
      $form_state->getValue(['general_informations', 'method'])
    );
    $endpoint->setSaveEntity(
      $form_state->getValue(['behaviour', 'saveEntity'])
    );
    $endpoint->setSaveInEntityType(
      $form_state->getValue(['behaviour', 'entities', 'saveInEntityType'])
    );
    $endpoint->setSaveInEntityBundle(
      $form_state->getValue(['behaviour', 'entities', 'saveInEntityBundle'])
    );
    $endpoint->setEntityParentElement(
      $form_state->getValue(['behaviour', 'entities', 'entityParentElement'])
    );
    $endpoint->setEntityUpdate(
      $form_state->getValue(['behaviour', 'entities', 'entityUpdate'])
    );
    $endpoint->setEntityDelete(
      $form_state->getValue(['behaviour', 'entities', 'entityDelete'])
    );
    $endpoint->setSaveFile(
      $form_state->getValue(['behaviour', 'saveFile'])
    );
    $endpoint->setIgnoreEmpty(
      $form_state->getValue(['behaviour', 'ignoreEmpty'])
    );

    $status = $endpoint->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created new endpoint %label.', ['%label' => $endpoint->label()]));
        $this->logger('wsm')->notice('Created new endpoint %label.', ['%label' => $endpoint->label()]);
        // $form_state->setRedirectUrl($endpoint->urlInfo('edit-form'));
        $form_state->setRedirect('entity.endpoint.edit_form', ['endpoint' => $endpoint->get('id')]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addMessage($this->t('Updated endpoint %label.', ['%label' => $endpoint->label()]));
        $this->logger('wsm')->notice('Updated endpoint %label.', ['%label' => $endpoint->label()]);
        // $form_state->setRedirectUrl($endpoint->urlInfo('edit-form'));
        $form_state->setRedirect('entity.endpoint.collection');
        break;
    }
  }

  /**
   * Helper function to check whether an Endpoint configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('endpoint')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

  /**
   * Helper function to get list of available entity types.
   */
  protected function getAvailableEntityTypes() {
    $entity_types = [];
    $entity_type_manager = \Drupal::service('entity_type.manager');

    // A list of entity types that are not supported.
    $unsupported_types = $this->getUnsupportedEntityTypes();

    foreach ($entity_type_manager->getDefinitions() as $entity_name => $definition) {
      // Skipping unsupported entities.
      if (in_array($entity_name, $unsupported_types)) {
        continue;
      }
      if ($definition instanceof ContentEntityType || $definition instanceof ConfigEntityType) {
        $entity_types[$entity_name] = $this->getEntityTypeLabel($definition);
      }
    }

    return $entity_types;
  }

  /**
   * Helper function to get list of unsupported entity types.
   */
  protected function getUnsupportedEntityTypes() {
    $unsupported_types = [
      'action',
      'base_field_override',
      'block',
      'block_content_type',
      'captcha_point',
      'configurable_language',
      'crop',
      'crop_type',
      'contact_message',
      'date_format',
      'editor',
      'endpoint',
      'entity_browser',
      'entity_form_mode',
      'entity_form_display',
      'entity_view_mode',
      'entity_view_display',
      'field_config',
      'field_storage_config',
      'filter_format',
      'field_mapping',
      'header',
      'image_style',
      'language_content_settings',
      'linkit_profile',
      'login_destination',
      'media_type',
      'menu',
      'menu_link_content',
      'node_type',
      'param',
      'paragraphs_type',
      'pathauto_pattern',
      'path_alias',
      'search_page',
      'shortcut',
      'taxonomy_vocabulary',
      'user_role',
      'view',
      'webform_options',
      'webform_submission',
      'webform',
    ];

    return $unsupported_types;
  }

  /**
   * Returns the label for the entity type specified.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type to process.
   *
   * @return string
   *   The entity type label.
   */
  protected function getEntityTypeLabel(EntityTypeInterface $entityType) {
    $label = $entityType->getLabel();
    if (is_a($label, 'Drupal\Core\StringTranslation\TranslatableMarkup')) {
      $label = $label->render();
    }

    return $label;
  }

  /**
   * Returns an array of available bundles of an entity type.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state.
   * @param \Drupal\wsm\Entity\Endpoint $entity
   *   The current endpoint.
   *
   * @return array
   *   A list of available bundles as $id => $label.
   */
  protected function getAvailableEntityBundle(FormStateInterface $form_state, Endpoint $entity) {
    $options = [];
    $entity_type = $form_state->getValue(
      ['behaviour', 'entities', 'saveInEntityType']
    );
    if (empty($entity_type)) {
      $entity_type = $entity->getSaveInEntityType();
    }
    if (!empty($entity_type)) {
      $bundle_info = \Drupal::service('entity_type.bundle.info');
      $bundles = $bundle_info->getBundleInfo($entity_type);
      foreach ($bundles as $bundle_id => $bundle_metadata) {
        $options[$bundle_id] = $bundle_metadata['label'];
      }
    }

    return $options;
  }

  /**
   * Ajax callback to update entity bundle list.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return an AjaxResponse
   */
  public function updateEntityBundleField(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#' . $this->getFormId() . '_entity_group', $form['behaviour']['entities']));
    return $response;
  }

}
