<?php

namespace Drupal\wsm\Form\Endpoint;

use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete an Endpoint.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save endpoint');
    $actions['delete']['#value'] = $this->t('Delete endpoint');
    return $actions;
  }

}
