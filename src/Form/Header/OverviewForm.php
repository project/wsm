<?php

namespace Drupal\wsm\Form\Header;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wsm\EndpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an overview form for headers on an endpoint.
 */
class OverviewForm extends FormBase {

  /**
   * The endpoint to which the headers are applied to.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  private $endpoint;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "wsm_header_overview_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EndpointInterface $endpoint = NULL) {
    $this->endpoint = $endpoint;
    $endpoint_id = $this->endpoint->id();

    $form['headers'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Header'),
          'colspan' => 2
        ],
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No headers added.'),
    ];
    $headers = \Drupal::entityTypeManager()->getStorage('header')->loadMultiple($this->endpoint->getHeaders());

    foreach ($headers as $header) {
      $key = $header->id();

      $form['headers'][$key]['id'] = [
        '#plain_text' => (string) str_replace($endpoint_id . '__header_', '', $header->id()),
      ];

      $form['headers'][$key]['label'] = [
        '#plain_text' => (string) $header->getName(),
      ];

      $form['headers'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];

      $form['headers'][$key]['operations']['#links']['edit'] = [
        'title' => t('Edit'),
        'url' => Url::fromRoute('entity.header.edit_form', [
          'endpoint' =>  $this->endpoint->id(),
          'header' => $key,
        ]),
      ];

      $form['headers'][$key]['operations']['#links']['delete'] = [
        'title' => t('Delete'),
        'url' => Url::fromRoute('entity.header.delete_form', [
          'endpoint' =>  $this->endpoint->id(),
          'header' => $key,
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->endpoint->save();
  }

}
