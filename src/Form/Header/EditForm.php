<?php

namespace Drupal\wsm\Form\Header;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form to edit header.
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save header');

    $actions['delete']['#url']->setRouteParameter('endpoint', \Drupal::routeMatch()->getParameter('endpoint'));
    $actions['delete']['#value'] = $this->t('Delete header');

    return $actions;
  }

}
