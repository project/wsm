<?php

namespace Drupal\wsm\Form\Header;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove a header from a endpoint.
 */
class DeleteForm extends EntityConfirmFormBase {

  /**
   * The endpoints that the header is applied to.
   *
   * @var \Drupal\wsm\EndpointInterface
   */
  protected $endpoint;

  /**
   * The header to be removed from the endpoint.
   *
   * @var \Drupal\wsm\HeaderInterface
   */
  protected $header;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());

    return $this->t('Are you sure you want to delete the @header header from the %endpoint endpoint?', ['%endpoint' => $endpoint->label(), '@header' => $this->entity->getName()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.headers', [
      'endpoint' => $this->entity->getEndpoint(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($this->entity->getEndpoint());
    $endpoint->removeHeader($this->entity->id());
    $this->entity->delete();

    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', array('%label' => $this->entity->label())));

    $this->logger('endpoint')->notice('The header %label has been deleted in the @endpoint endpoint.', [
      '%label' => $this->entity->label(),
      '@endpoint' => $endpoint->label(),
    ]);

    $form_state->setRedirect('entity.headers', [
      'endpoint' => $endpoint->id(),
    ]);

  }

}
