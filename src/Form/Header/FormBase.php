<?php

namespace Drupal\wsm\Form\Header;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wsm\EndpointInterface;

/**
 * Base form for header add and edit forms.
 */
class FormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\wsm\HeaderInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $endpoint_id = \Drupal::routeMatch()->getParameter('endpoint');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Header name'),
      '#default_value' => $this->entity->getName(),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#field_prefix' => ($this->entity->isNew()) ? $endpoint_id . '__header_' : '',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\wsm\Entity\Header', 'load']
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Header value'),
      '#default_value' => $this->entity->getValue(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $endpoint_id = \Drupal::routeMatch()->getParameter('endpoint');
    $header = $this->entity;

    if ($header->isNew()) {
      $header->id = $endpoint_id . '__header_' . $header->id();
      $header->setEndpoint($endpoint_id);

      $endpoint = \Drupal::entityTypeManager()->getStorage('endpoint')->load($endpoint_id);
      $endpoint->addHeader($header->id());
      $endpoint->save();
    }

    $status = $header->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created new header %name.', ['%name' => $header->getName()]));
        $this->logger('wsm')->notice('Created new header %name.', ['%name' => $header->getName()]);
        break;

      case SAVED_UPDATED:
        \Drupal::messenger()->addMessage($this->t('Updated header %name.', ['%name' => $header->getName()]));
        $this->logger('wsm')->notice('Updated endpoint %name.', ['%name' => $header->getName()]);
        break;
    }

    $form_state->setRedirect('entity.headers', ['endpoint' => $header->getEndpoint()]);
  }

  /**
   * Helper function to check whether an Endpoint configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('header')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
