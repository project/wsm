<?php

namespace Drupal\wsm;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a endpoint entity.
 */
interface EndpointInterface extends ConfigEntityInterface {

  /**
   * Gets the endpoint description.
   *
   * @return string
   *   The endpoint description.
   */
  public function getDescription();

  /**
   * Sets the endpoint description.
   *
   * @param string $description
   *   The endpoint description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the endpoint base url.
   *
   * @return string
   *   The endpoint base url.
   */
  public function getEndpointBaseUrl();

  /**
   * Sets the endpoint base url.
   *
   * @param string $endpointBaseUrl
   *   The endpoint base url.
   *
   * @return $this
   */
  public function setEndpointBaseUrl($endpointBaseUrl);

  /**
   * Gets the endpoint response type.
   *
   * @return string
   *   The endpoint response type.
   */
  public function getResponseType();

  /**
   * Sets the endpoint response type.
   *
   * @param string $responseType
   *   The endpoint response type.
   *
   * @return $this
   */
  public function setResponseType($responseType);

  /**
   * Gets the endpoint response type.
   *
   * @return string
   *   The endpoint response type.
   */
  public function getMethod();

  /**
   * Sets the endpoint method.
   *
   * @param string $method
   *   The endpoint method.
   *
   * @return $this
   */
  public function setMethod($method);

  /**
   * Gets the endpoint save in entity boolean.
   *
   * @return bool
   *   The endpoint save in entity boolean.
   */
  public function getSaveEntity();

  /**
   * Sets the endpoint save in entity boolean.
   *
   * @param bool $saveEntity
   *   The endpoint save in entity boolean.
   *
   * @return $this
   */
  public function setSaveEntity($saveEntity);

  /**
   * Gets the entity type to save in.
   *
   * @return string
   *   The entity type to save in.
   */
  public function getSaveInEntityType();

  /**
   * Sets the entity type to save in.
   *
   * @param string $saveInEntityType
   *   The the entity type to save in.
   *
   * @return $this
   */
  public function setSaveInEntityType($saveInEntityType);

  /**
   * Gets the entity bundle to save in.
   *
   * @return string
   *   The entity bundle to save in.
   */
  public function getSaveInEntityBundle();

  /**
   * Sets the entity bundle to save in.
   *
   * @param string $saveInEntityBundle
   *   The entity bundle to save in.
   *
   * @return $this
   */
  public function setSaveInEntityBundle($saveInEntityBundle);

  /**
   * Gets the entity parent element in response.
   *
   * @return string
   *   The entity parent element in response.
   */
  public function getEntityParentElement();

  /**
   * Sets the entity parent element in response.
   *
   * @param string $entityParentElement
   *   The entity parent element in response.
   *
   * @return $this
   */
  public function setEntityParentElement($entityParentElement);

  /**
   * Gets the entity update flag.
   *
   * @return bool
   *   The entity update flag.
   */
  public function getEntityUpdate();

  /**
   * Sets the entity update flag.
   *
   * @param bool $entityUpdate
   *   The entity update flag.
   *
   * @return $this
   */
  public function setEntityUpdate($entityUpdate);

  /**
   * Gets the endpoint save in file boolean.
   *
   * @return bool
   *   The endpoint save in file boolean.
   */
  public function getSaveFile();

  /**
   * Sets the endpoint save in file boolean.
   *
   * @param bool $saveFile
   *   The endpoint save in file boolean.
   *
   * @return $this
   */
  public function setSaveFile($saveFile);

  /**
   * Gets the entity id as last path boolean.
   *
   * @return bool
   *   The entity id as last path boolean.
   */
  public function getLastPathEntityId();

  /**
   * Sets the entity id as last path boolean.
   *
   * @param bool $lastPathEntityId
   *   The entity id as last path boolean.
   *
   * @return $this
   */
  public function setLastPathEntityId($lastPathEntityId);

  /**
   * Gets the endpoint ignore actions if empty boolean.
   *
   * @return bool
   *   The endpoint ignore actions if empty boolean.
   */
  public function getIgnoreEmpty();

  /**
   * Sets the endpoint ignore actions if empty boolean.
   *
   * @param bool $ignoreEmpty
   *   The endpoint ignore actions if empty boolean.
   *
   * @return $this
   */
  public function setIgnoreEmpty($ignoreEmpty);

  /**
   * Gets the endpoint delete entities boolean.
   *
   * @return bool
   *   The endpoint delete entities boolean.
   */
  public function getEntityDelete();

  /**
   * Sets the endpoint delete entities boolean.
   *
   * @param bool $entityDelete
   *   The endpoint delete entities boolean.
   *
   * @return $this
   */
  public function setEntityDelete($entityDelete);
}
