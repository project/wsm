<?php

namespace Drupal\wsm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\wsm\EndpointInterface;

/**
 * Defines the endpoint entity.
 *
 * @ConfigEntityType(
 *   id = "endpoint",
 *   label = @Translation("WSM Endpoint"),
 *   handlers = {
 *     "list_builder" = "Drupal\wsm\Controller\EndpointListBuilder",
 *     "form" = {
 *       "add" = "Drupal\wsm\Form\Endpoint\AddForm",
 *       "edit" = "Drupal\wsm\Form\Endpoint\EditForm",
 *       "delete" = "Drupal\wsm\Form\Endpoint\DeleteForm"
 *     }
 *   },
 *   admin_permission = "administer wsm",
 *   config_prefix = "endpoint",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/wsm",
 *     "edit-form" = "/admin/config/services/wsm/manage/{endpoint}",
 *     "delete-form" = "/admin/config/services/wsm/manage/{endpoint}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "endpointBaseUrl",
 *     "responseType",
 *     "method",
 *     "saveEntity",
 *     "saveInEntityType",
 *     "saveInEntityBundle",
 *     "entityParentElement",
 *     "entityUpdate",
 *     "entityDelete",
 *     "saveFile",
 *     "lastPathEntityId",
 *     "ignoreEmpty",
 *     "headers",
 *     "params",
 *     "field_mappings"
 *   }
 * )
 */
class Endpoint extends ConfigEntityBase implements EndpointInterface {

  /**
   * The ID of this endpoint.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of this endpoint.
   *
   * @var string
   */
  protected $label;

  /**
   * Description of this endpoint.
   *
   * @var string
   */
  protected $description;

  /**
   * Base URL of this endpoint.
   *
   * @var string
   */
  protected $endpointBaseUrl;

  /**
   * Response type of this endpoint.
   *
   * @var string
   */
  protected $responseType;

  /**
   * Save response in content types.
   *
   * @var bool
   */
  protected $saveEntity;

  /**
   * Entity type to save in.
   *
   * @var string
   */
  protected $saveInEntityType;

  /**
   * Entity bundle to save in.
   *
   * @var string
   */
  protected $saveInEntityBundle;

  /**
   * First level object containing entity.
   *
   * @var string
   */
  protected $entityParentElement;

  /**
   * Boolean to manage update existing entity.
   *
   * @var bool
   */
  protected $entityUpdate;

  /**
   * Save response in a text file.
   *
   * @var bool
   */
  protected $saveFile;

  /**
   * Add source entity id as last path of the endpoint.
   *
   * @var bool
   */
  protected $lastPathEntityId;

  /**
   * Don't do amy action if empty response.
   *
   * @var bool
   */
  protected $ignoreEmpty;

  /**
   * Configured headers for this endpoint.
   *
   * An associative array of headers machine name.
   *
   * @var array
   */
  protected $headers = [];

  /**
   * Configured params for this endpoint.
   *
   * An associative array of params machine name.
   *
   * @var array
   */
  protected $params = [];

  /**
   * Configured field mappings for this endpoint.
   *
   * An associative array of field mappings machine name.
   *
   * @var array
   */
  protected $field_mappings = [];

  /**
   * Delete entities that are no more in API results.
   *
   * @var bool
   */
  protected $entityDelete;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description');
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', trim($description));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointBaseUrl() {
    return $this->get('endpointBaseUrl');
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpointBaseUrl($endpointBaseUrl) {
    $this->set('endpointBaseUrl', trim($endpointBaseUrl));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseType() {
    return $this->get('responseType');
  }

  /**
   * {@inheritdoc}
   */
  public function setResponseType($responseType) {
    $this->set('responseType', trim($responseType));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->get('method');
  }

  /**
   * {@inheritdoc}
   */
  public function setMethod($method) {
    $this->set('method', trim($method));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSaveEntity() {
    return $this->get('saveEntity');
  }

  /**
   * {@inheritdoc}
   */
  public function setSaveEntity($saveEntity) {
    $this->set('saveEntity', $saveEntity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSaveInEntityType() {
    return $this->get('saveInEntityType');
  }

  /**
   * {@inheritdoc}
   */
  public function setSaveInEntityType($saveInEntityType) {
    $this->set('saveInEntityType', $saveInEntityType);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSaveInEntityBundle() {
    return $this->get('saveInEntityBundle');
  }

  /**
   * {@inheritdoc}
   */
  public function setSaveInEntityBundle($saveInEntityBundle) {
    $this->set('saveInEntityBundle', $saveInEntityBundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityParentElement() {
    return $this->get('entityParentElement');
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityParentElement($entityParentElement) {
    $this->set('entityParentElement', $entityParentElement);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityUpdate() {
    return $this->get('entityUpdate');
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityUpdate($entityUpdate) {
    $this->set('entityUpdate', $entityUpdate);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSaveFile() {
    return $this->get('saveFile');
  }

  /**
   * {@inheritdoc}
   */
  public function setSaveFile($saveFile) {
    $this->set('saveFile', $saveFile);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastPathEntityId() {
    return $this->get('lastPathEntityId');
  }

  /**
   * {@inheritdoc}
   */
  public function setLastPathEntityId($lastPathEntityId) {
    $this->set('lastPathEntityId', $lastPathEntityId);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIgnoreEmpty() {
    return $this->get('ignoreEmpty');
  }

  /**
   * {@inheritdoc}
   */
  public function setIgnoreEmpty($ignoreEmpty) {
    $this->set('ignoreEmpty', $ignoreEmpty);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader($header_id) {
    return $this->headers[$header_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * {@inheritdoc}
   */
  public function addHeader($header_id) {
    $this->headers[$header_id] = $header_id;
  }

  /**
   * {@inheritdoc}
   */
  public function removeHeader($header_id) {
    unset($this->headers[$header_id]);
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParam($param_id) {
    return $this->params[$param_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function addParam($param_id) {
    $this->params[$param_id] = $param_id;
  }

  /**
   * {@inheritdoc}
   */
  public function removeParam($param_id) {
    unset($this->params[$param_id]);
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapping($field_mapping_id) {
    return $this->field_mappings[$field_mapping_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMappings() {
    return $this->field_mappings;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldMapping($field_mapping_id) {
    $this->field_mappings[$field_mapping_id] = $field_mapping_id;
  }

  /**
   * {@inheritdoc}
   */
  public function removeFieldMapping($field_mapping_id) {
    unset($this->field_mappings[$field_mapping_id]);
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityDelete() {
    return $this->get('entityDelete');
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityDelete($entityDelete) {
    $this->set('entityDelete', $entityDelete);
    return $this;
  }

}
