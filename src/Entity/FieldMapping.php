<?php

namespace Drupal\wsm\Entity;

use Drupal\wsm\FieldMappingInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the fieldMapping entity.
 *
 * @ConfigEntityType(
 *   id = "field_mapping",
 *   label = @Translation("WSM Field mapping"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\wsm\Form\FieldMapping\AddForm",
 *       "edit" = "Drupal\wsm\Form\FieldMapping\EditForm",
 *       "delete" = "Drupal\wsm\Form\FieldMapping\DeleteForm"
 *     }
 *   },
 *   admin_permission = "administer wsm",
 *   config_prefix = "field_mapping",
 *   entity_keys = {
 *     "id" = "id",
 *     "endpoint_id" = "endpoint_id"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/wsm/manage/{endpoint}/field_mappings",
 *     "edit-form" = "/admin/config/services/wsm/manage/{endpoint}/field_mappings/{field_mapping}",
 *     "delete-form" = "/admin/config/services/wsm/manage/{endpoint}/field_mappings/{field_mapping}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "endpoint",
 *     "src_field",
 *     "dest_field",
 *     "is_id",
 *     "is_value",
 *     "is_tolowerstring",
 *     "use_on_delete",
 *     "is_referencebydistantid",
 *     "reference_origial_id_field"
 *   }
 * )
 */
class FieldMapping extends ConfigEntityBase implements FieldMappingInterface {

  /**
   * Associated Endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The machine name of the field mapping.
   *
   * @var string
   */
  public $id;

  /**
   * The source field of the field mapping.
   *
   * @var string
   */
  protected $src_field;

  /**
   * The destination field of the field mapping.
   *
   * @var string
   */
  public $dest_field;

  /**
   * The is id flag.
   *
   * @var bool
   */
  public $is_id;

  /**
   * The is value flag.
   *
   * This field is used to set a value to the imported entity.
   *
   * @var bool
   */
  public $is_value;

  /**
   * The is to lower string flag.
   *
   * This field is used to save endpoint item value to lower string.
   *
   * @var bool
   */
  public $is_tolowerstring;

  /**
   * The use on delete flag.
   *
   * This field is as a condition on delete process. Only for value field.
   *
   * @var bool
   */
  public $use_on_delete;

  /**
   * The is reference by distant id flag.
   *
   * This field is used to get referenced entity by distant object id.
   *
   * @var bool
   */
  public $is_referencebydistantid;

  /**
   * The reference orginal id field name.
   *
   * This field is used to get the referenced entity by the field name given.
   *
   * @var string
   */
  public $reference_origial_id_field;

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->get('endpoint');
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpoint($endpoint) {
    $this->set('endpoint', $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceField() {
    return $this->get('src_field');
  }

  /**
   * {@inheritdoc}
   */
  public function setSourceField($src_field) {
    $this->set('src_field', trim($src_field));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationField() {
    return $this->get('dest_field');
  }

  /**
   * {@inheritdoc}
   */
  public function setDestinationField($dest_field) {
    $this->set('dest_field', trim($dest_field));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsIdField() {
    return $this->get('is_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setIsIdField($is_id) {
    $this->set('is_id', $is_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsValueField() {
    return $this->get('is_value');
  }

  /**
   * {@inheritdoc}
   */
  public function setIsValueField($is_value) {
    $this->set('is_value', $is_value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsToLowerString() {
    return $this->get('is_tolowerstring');
  }

  /**
   * {@inheritdoc}
   */
  public function setIsToLowerString($is_tolowerstring) {
    $this->set('is_tolowerstring', $is_tolowerstring);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUseOnDelete() {
    return $this->get('use_on_delete');
  }

  /**
   * {@inheritdoc}
   */
  public function setUseOnDelete($use_on_delete) {
    $this->set('use_on_delete', $use_on_delete);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsReferenceByDistantId() {
    return $this->get('is_referencebydistantid');
  }

  /**
   * {@inheritdoc}
   */
  public function setIsReferenceByDistantId($is_referencebydistantid) {
    $this->set('is_referencebydistantid', $is_referencebydistantid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceOriginalIdField() {
    return $this->get('reference_origial_id_field');
  }

  /**
   * {@inheritdoc}
   */
  public function setReferenceOriginalIdField($reference_origial_id_field) {
    $this->set('reference_origial_id_field', $reference_origial_id_field);
    return $this;
  }

}
