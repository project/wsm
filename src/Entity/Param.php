<?php

namespace Drupal\wsm\Entity;

use Drupal\wsm\ParamInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the param entity.
 *
 * @ConfigEntityType(
 *   id = "param",
 *   label = @Translation("WSM Param"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\wsm\Form\Param\AddForm",
 *       "edit" = "Drupal\wsm\Form\Param\EditForm",
 *       "delete" = "Drupal\wsm\Form\Param\DeleteForm"
 *     }
 *   },
 *   admin_permission = "administer wsm",
 *   config_prefix = "param",
 *   entity_keys = {
 *     "id" = "id",
 *     "endpoint_id" = "endpoint_id",
 *     "name" = "name"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/wsm/manage/{endpoint}/params",
 *     "edit-form" = "/admin/config/services/wsm/manage/{endpoint}/params/{param}",
 *     "delete-form" = "/admin/config/services/wsm/manage/{endpoint}/params/{param}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "endpoint",
 *     "label",
 *     "value"
 *   }
 * )
 */
class Param extends ConfigEntityBase implements ParamInterface {

  /**
   * Associated Endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The machine name of the param.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the param.
   *
   * @var string
   */
  protected $label;

  /**
   * The value of the param.
   *
   * @var string
   */
  public $value;

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->get('endpoint');
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpoint($endpoint) {
    $this->set('endpoint', $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('label');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('label', trim($name));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->get('value');
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value) {
    $this->set('value', trim($value));
    return $this;
  }

}
