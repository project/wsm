<?php

namespace Drupal\wsm\Entity;

use Drupal\wsm\HeaderInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the header entity.
 *
 * @ConfigEntityType(
 *   id = "header",
 *   label = @Translation("WSM Header"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\wsm\Form\Header\AddForm",
 *       "edit" = "Drupal\wsm\Form\Header\EditForm",
 *       "delete" = "Drupal\wsm\Form\Header\DeleteForm"
 *     }
 *   },
 *   admin_permission = "administer wsm",
 *   config_prefix = "header",
 *   entity_keys = {
 *     "id" = "id",
 *     "endpoint_id" = "endpoint_id",
 *     "name" = "name"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/wsm/manage/{endpoint}/headers",
 *     "edit-form" = "/admin/config/services/wsm/manage/{endpoint}/headers/{header}",
 *     "delete-form" = "/admin/config/services/wsm/manage/{endpoint}/headers/{header}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "endpoint",
 *     "label",
 *     "value"
 *   }
 * )
 */
class Header extends ConfigEntityBase implements HeaderInterface {

  /**
   * Associated Endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The machine name of the header.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the header.
   *
   * @var string
   */
  protected $label;

  /**
   * The value of the header.
   *
   * @var string
   */
  public $value;

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->get('endpoint');
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpoint($endpoint) {
    $this->set('endpoint', $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('label');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('label', trim($name));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->get('value');
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value) {
    $this->set('value', trim($value));
    return $this;
  }

}
