CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Webservice Manager is a configuration manager for webservices calls.

WSM allows to fully set parameters for third party service API call and retrieve data (headers, parameters, ...).
Call results can be directly rendered in a template or imported as node.

WSM comes with a field type that permit call of the API on node rendering.

You can :
 * Create webservice entites with Headers and Parameters 
 * Export / Import with Drupal configuration 
 * Map webservice response to content type 
 * Add specific actions on fields (download image, strip tags on text field, ...)
 * Render webservice response using a template 
 * Regulary import content using the cron


REQUIREMENTS
------------

This module requires the following module:

 * JSON API - https://www.drupal.org/project/jsonapi


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 1. Configure the module at Administration > Configuration > Web services > Web services manager (admin/config/services/wsm). Add a new endpoint by clicking "Add endpoint".
 2. Fill out the form.
 3. Click "Save" to save your endpoint.
 4. Click on Headers tab to access your enpoint list of headers.
 5. Add or edit the headers.
 6. Click on Parameters tab to access your enpoint list of parameters.
 7. Add or edit the parameters.
 8. Use the cron or the field to call the endpoint.
 

MAINTAINERS
-----------

Current maintainers:

 * Hideo Kubota - https://www.drupal.org/u/hideokubota
 * Julien Gautier - https://www.drupal.org/u/julien_g